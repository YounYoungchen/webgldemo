/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-01 11:52:23
 * @LastEditTime: 2021-10-07 23:53:32
 */
import mat4 from "gl-mat4";
import Vector3 from "./helper/Vector3";

export default class RenderObj {
  constructor() {
    this.rotate = new Vector3({ x: 0, y: 0, z: 0 });
    this.position = new Vector3({ x: 0, y: 0, z: -1 });
    this.scale = new Vector3({ x: 1, y: 1, z: 1 });
  }

  bind(gl, camera) {
    this.gl = gl;
    this.shaderProgram = this._setUpProgram(gl);
    this.programInfo = {
      program: this.shaderProgram,
      attribLocations: {
        position: this.gl.getAttribLocation(this.shaderProgram, "position"),
        color: gl.getAttribLocation(this.shaderProgram, "color"),
      },
      uniformLocations: {
        model: this.gl.getUniformLocation(this.shaderProgram, "model"),
        viewPosition: this.gl.getUniformLocation(this.shaderProgram, "view"),
        projection: this.gl.getUniformLocation(
          this.shaderProgram,
          "projection"
        ),
      },
    };
    gl.enable(gl.DEPTH_TEST);

    this.drawBuffer = this._getShapeBuffer(gl);
    this.camera = camera;
  }

  _setUpProgram(gl) {
    const vertexShader = this._getVertextShader(gl);
    const fragmentShader = this._getFragmentShader(gl);
    const vShader = vertexShader.compile();
    const fShader = fragmentShader.compile();

    // 创建着色器程序
    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vShader);
    gl.attachShader(shaderProgram, fShader);
    gl.linkProgram(shaderProgram);

    // 创建失败， alert
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert(
        "Unable to initialize the shader program: " +
          gl.getProgramInfoLog(shaderProgram)
      );
      return null;
    }
    return shaderProgram;
  }

  setRotate(x, y, z) {
    this.rotate.x += x;
    this.rotate.y += y;
    this.rotate.z += z;
  }

  setPosition(x, y, z) {
    this.position.x = x;
    this.position.y = y;
    this.position.z = z;
  }

  setScale(x, y, z) {
    this.scale.x = x;
    this.scale.y = y;
    this.scale.z = z;
  }

  updateAttrsAndUniforms() {
    const gl = this.gl;
    this.modelViewMatrix = mat4.create();
    //位置
    mat4.translate(
      this.modelViewMatrix, // destination matrix
      this.modelViewMatrix, // matrix to translate
      this.position.toArray()
    ); // amount to translate

    //旋转
    mat4.rotateX(this.modelViewMatrix, this.modelViewMatrix, this.rotate.x);
    mat4.rotateY(this.modelViewMatrix, this.modelViewMatrix, this.rotate.y);
    mat4.rotateZ(this.modelViewMatrix, this.modelViewMatrix, this.rotate.z);

    //缩放
    mat4.scale(
      this.modelViewMatrix,
      this.modelViewMatrix,
      this.scale.toArray()
    );

    //pass transform matrix data to OpenGl program var: model
    gl.uniformMatrix4fv(
      this.programInfo.uniformLocations.model,
      false,
      new Float32Array(this.modelViewMatrix)
    );

    gl.uniformMatrix4fv(
      this.programInfo.uniformLocations.projection,
      false,
      new Float32Array(this.camera.getMatrix())
    );

    gl.uniformMatrix4fv(
      this.programInfo.uniformLocations.viewPosition,
      false,
      new Float32Array(this.camera.getViewMatrix())
    );

    // Set the positions attribute
    gl.enableVertexAttribArray(this.programInfo.attribLocations.position);
    gl.bindBuffer(gl.ARRAY_BUFFER, this.drawBuffer.position);
    gl.vertexAttribPointer(
      this.programInfo.attribLocations.position,
      3,
      gl.FLOAT,
      false,
      0,
      0
    );

    // Set the colors attribute
    gl.enableVertexAttribArray(this.programInfo.attribLocations.color);
    gl.bindBuffer(gl.ARRAY_BUFFER, this.drawBuffer.color);
    gl.vertexAttribPointer(
      this.programInfo.attribLocations.color,
      4,
      gl.FLOAT,
      false,
      0,
      0
    );

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.drawBuffer.indices);
  }

  draw() {
    this.gl.useProgram(this.shaderProgram);
    this.updateAttrsAndUniforms();
    this.gl.drawElements(
      this.gl.TRIANGLES,
      this._getVertexCount(),
      this.gl.UNSIGNED_SHORT,
      0
    );
  }

  _getShapeBuffer(gl) {}
  _getVertexCount() {}
  _getVertextShader(gl) {}
  _getFragmentShader(gl) {}
}
