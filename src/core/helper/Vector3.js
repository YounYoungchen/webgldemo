/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-07 23:41:37
 * @LastEditTime: 2021-10-07 23:41:37
 */
export default class Vactor3 {
  constructor({ x, y, z } = {}) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  toArray() {
    return [this.x, this.y, this.z];
  }

  static indentity() {
    return new Vactor3({ x: 1, y: 1, z: 1 });
  }
}
