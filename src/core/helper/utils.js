/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-02 15:05:35
 * @LastEditTime: 2021-10-02 15:10:19
 */

const isPowerOf2 = (value) => {
  return (value & (value - 1)) == 0;
};

export default isPowerOf2;
