/*
 * @Author: Younchen
 * @Description: 顶点着色器， 片段着色器
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-01 11:46:33
 * @LastEditTime: 2021-10-01 11:46:34
 */
export default class Shader {
  constructor(gl, type, source) {
    this.gl = gl;
    this.type = type;
    this.source = source;
  }

  compile() {
    const shader = this.gl.createShader(this.type);
    // Send the source to the shader object
    this.gl.shaderSource(shader, this.source);
    // Compile the shader program
    this.gl.compileShader(shader);
    // See if it compiled successfully

    if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
      alert(
        "An error occurred compiling the shaders: " +
          this.gl.getShaderInfoLog(shader)
      );
      this.gl.deleteShader(shader);
      return null;
    }
    return shader;
  }
}
