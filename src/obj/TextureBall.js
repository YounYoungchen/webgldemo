/*
 * @Author: Younchen
 * @Description: 正方体
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-01 18:11:00
 * @LastEditTime: 2021-10-07 02:01:25
 */
import { RenderObjOld, Shader } from "../core";

export default class TextureBall extends RenderObjOld {
  constructor({ textureImg }) {
    super();
    this.textureImg = textureImg;
  }

  _initBuffers(gl) {
    this.texture = this.loadTexture(gl, this.textureImg);
    //使用三十条纬线和三十条经线划分球体
    var latitudeBands = 250;
    var longitudeBands = 250;
    //球体半径
    var radius = 2;
    //存储顶点坐标
    var vertexPositionData = [];
    //存储纹理坐标
    var textureCoordData = [];
    //从纬线开始遍历
    var indices = [];
    for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
      //计算θ角度
      var theta = (latNumber * Math.PI) / latitudeBands;
      var sinTheta = Math.sin(theta);
      var cosTheta = Math.cos(theta);

      for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
        //计算φ角度
        var phi = (longNumber * 2 * Math.PI) / longitudeBands;
        var sinPhi = Math.sin(phi);
        var cosPhi = Math.cos(phi);
        //计算顶点的x,y,z坐标
        var x = radius * cosPhi * sinTheta;
        var y = radius * cosTheta;
        var z = radius * sinPhi * sinTheta;
        //贴图是矩形的，我们将贴图在X轴上按照经线划分，在Y轴上按照纬线划分，来计算顶点对应的贴图U，V坐标
        var u = longNumber / longitudeBands;
        var v = latNumber / latitudeBands;

        textureCoordData.push(u);
        textureCoordData.push(v);
        vertexPositionData.push(x);
        vertexPositionData.push(y);
        vertexPositionData.push(z);

        if (latNumber < latitudeBands && longNumber < longitudeBands) {
          const A = latNumber * (latitudeBands + 1) + longNumber;
          const B = A + (latitudeBands + 1);
          const C = A + 1;
          const D = B + 1;

          indices.push(A);
          indices.push(B);
          indices.push(C);

          indices.push(C);
          indices.push(B);
          indices.push(D);
        }
      }
    }

    this.vertextCount = indices.length;

    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(vertexPositionData),
      gl.STATIC_DRAW
    );

    const textureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
    gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(textureCoordData),
      gl.STATIC_DRAW
    );

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(
      gl.ELEMENT_ARRAY_BUFFER,
      new Uint16Array(indices),
      gl.STATIC_DRAW
    );

    // const faceColors = [
    //   [1.0, 1.0, 1.0, 1.0], // Front face: white
    //   [1.0, 0.0, 0.0, 1.0], // Back face: red
    //   [0.0, 1.0, 0.0, 1.0], // Top face: green
    //   [0.0, 0.0, 1.0, 1.0], // Bottom face: blue
    //   [1.0, 1.0, 0.0, 1.0], // Right face: yellow
    //   [1.0, 0.0, 1.0, 1.0], // Left face: purple
    // ];
    // const colorsData = [];
    // for (let i = 0; i <= latitudeBands; i++) {
    //   for (let j = 0; j <= latitudeBands; j++) {
    //     colorsData.push(faceColors[i % 4][0]);
    //     colorsData.push(faceColors[i % 4][1]);
    //     colorsData.push(faceColors[i % 4][2]);
    //     colorsData.push(faceColors[i % 4][3]);
    //   }
    // }

    // const colorBuffer = gl.createBuffer();
    // gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    // gl.bufferData(
    //   gl.ARRAY_BUFFER,
    //   new Float32Array(colorsData),
    //   gl.STATIC_DRAW
    // );

    return {
      position: positionBuffer,
      textureCoord: textureCoordBuffer,
      index: indexBuffer,
      // color: colorBuffer,
    };
  }

  _getTexture(gl) {
    return this.texture;
  }

  _getFragmentShader(gl) {
    return new Shader(gl, gl.VERTEX_SHADER, vsSource);
  }

  _getVertextShader(gl) {
    return new Shader(gl, gl.FRAGMENT_SHADER, fsSource);
  }

  _glDraw(gl) {
    {
      const vertexCount = this.vertextCount;
      const type = gl.UNSIGNED_SHORT;
      const offset = 0;
      gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }
  }
}

// Vertex shader program
// uModelViewMatrix 模型矩阵
// uProjectionMatrix 投影矩阵
const vsSource = `
//attribute 变量只能由vertex shader 中使用
attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;

//uniform 变量是外部程序传递给（vertex和fragment）shader的变量
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

//由vertex shader变量修改，fragemnt shader来使用
//两个shader的变量名要统一
varying highp vec2 vTextureCoord;

void main(void) {
  gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
  vTextureCoord = aTextureCoord;
}
  `;

//片段着色器
const fsSource = `
varying highp vec2 vTextureCoord;

uniform sampler2D uSampler;

void main(void) {
  gl_FragColor = texture2D(uSampler, vTextureCoord);
}
`;
