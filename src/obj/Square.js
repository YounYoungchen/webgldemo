import { RenderObjOld, Shader } from "../core";
/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-01 17:11:07
 * @LastEditTime: 2021-10-06 23:18:33
 */
export default class Square extends RenderObjOld {
  _initBuffers(gl) {
    //createBuffer() 创建缓冲区，用来保存顶点坐标
    const positionBuffer = gl.createBuffer();
    //bindBuffer() 函数绑定上下文。
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    //定义坐标
    const vertexs = [
      1.0,
      1.0,
      0.0,
      -1.0,
      1.0,
      0.0,
      1.0,
      -1.0,
      0.0,
      -1.0,
      -1.0,
      0.0,
    ];
    //将坐标传递给gl 建立对象的顶点
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexs), gl.STATIC_DRAW);

    return {
      position: positionBuffer,
    };
  }

  _getFragmentShader(gl) {
    return new Shader(gl, gl.VERTEX_SHADER, vsSource);
  }

  _getVertextShader(gl) {
    return new Shader(gl, gl.FRAGMENT_SHADER, fsSource);
  }

  _glDraw(gl) {
    {
      const offset = 0;
      const vertexCount = 4;
      gl.drawArrays(gl.TRIANGLE_STRIP, offset, vertexCount);
    }
  }
}

// Vertex shader program
// uModelViewMatrix 模型矩阵
// uProjectionMatrix 投影矩阵
const vsSource = `
  attribute vec4 aVertexPosition;

  uniform mat4 uModelViewMatrix;
  uniform mat4 uProjectionMatrix;

  void main() {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
  }
`;

//片段着色器
const fsSource = `
    void main() {
      gl_FragColor = vec4(1.0, 0.6, 0.5, 1.0);
    }
  `;
