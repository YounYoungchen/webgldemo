/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-06 14:51:48
 * @LastEditTime: 2021-10-06 14:52:51
 */
const ClipwordGlView = () => {
  const canvas = document.getElementById("canvas");
  this.canvas.width = window.innerWidth;
  this.canvas.height = window.innerHeight;
  this.gl = MDN.createContext(canvas);
  return <div></div>;
};

export default ClipwordGlView;
