import MDN from "../shared/MDN";

import { useEffect } from "react";
/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-10-06 14:58:24
 * @LastEditTime: 2021-10-06 17:46:58
 */

class WebGLBox {
  constructor({ canvas } = {}) {
    this.canvas = canvas;
    this._init();
  }

  _init() {
    this.gl = MDN.createContext(this.canvas);
    var gl = this.gl;
    this.webglProgram = MDN.createWebGLProgramFromIds(
      gl,
      "vertex-shader",
      "fragment-shader"
    );
    gl.useProgram(this.webglProgram);

    // Save the attribute and uniform locations
    this.positionLocation = gl.getAttribLocation(this.webglProgram, "position");
    this.colorLocation = gl.getUniformLocation(this.webglProgram, "color");

    // Tell WebGL to test the depth when drawing, so if a square is behind
    // another square it won't be drawn
    gl.enable(gl.DEPTH_TEST);

    WebGLBox.prototype.draw = function (settings) {
      // Create some attribute data, these are the triangles that will end being
      // drawn to the screen. There are two that form a square.

      var data = new Float32Array([
        //Triangle 1
        settings.left,
        settings.bottom,
        settings.depth,
        settings.w,
        settings.right,
        settings.bottom,
        settings.depth,
        settings.w,
        settings.left,
        settings.top,
        settings.depth,
        settings.w,

        //Triangle 2
        settings.left,
        settings.top,
        settings.depth,
        settings.w,
        settings.right,
        settings.bottom,
        settings.depth,
        settings.w,
        settings.right,
        settings.top,
        settings.depth,
        settings.w,
      ]);

      // Use WebGL to draw this onto the screen.

      // Performance Note: Creating a new array buffer for every draw call is slow.
      // This function is for illustration purposes only.

      var gl = this.gl;

      // Create a buffer and bind the data
      var buffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
      gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);

      // Setup the pointer to our attribute data (the triangles)
      gl.enableVertexAttribArray(this.positionLocation);
      gl.vertexAttribPointer(this.positionLocation, 4, gl.FLOAT, false, 0, 0);

      // Setup the color uniform that will be shared across all triangles
      gl.uniform4fv(this.colorLocation, settings.color);

      // Draw the triangles to the screen
      gl.drawArrays(gl.TRIANGLES, 0, 6);
    };
  }
}

const WebGLSampleView = () => {
  useEffect(() => {
    const canvas = document.querySelector("#canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    const v = new WebGLBox({ canvas: canvas });
    v.draw({
      top: 0.5, // x
      bottom: -0.5, // x
      left: -0.5, // y
      right: 0.5, // y
      w: 2,
      depth: 0, // z

      color: [1, 0.4, 0.4, 1], // red
    });

    //Draw a green box up top
    v.draw({
      top: 0.9, // x
      bottom: 0, // x
      left: -0.9, // y
      right: 0.9, // y
      w: 1.2,

      depth: 0, // z
      color: [0.4, 1, 0.4, 1], // green
    });

    // This box doesn't get drawn because it's outside of clip space. The depth is
    // outside of the -1.0 to 1.0 range.
    v.draw({
      top: 1, // x
      bottom: -1, // x
      left: -1, // y
      right: 1, // y
      w: 2,
      depth: 0.8, // z
      color: [0.4, 0.4, 1, 1], // blue
    });
  });
  return (
    <div>
      <canvas id="canvas"></canvas>
    </div>
  );
};

export default WebGLSampleView;
