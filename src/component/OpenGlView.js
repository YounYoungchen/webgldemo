/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-09-30 23:59:18
 * @LastEditTime: 2021-10-08 00:42:14
 */

import { useEffect } from "react";
import { Square, Cube, TextureCube, TextureBall } from "../obj";
import mat4 from "gl-mat4";
import Vactor3 from "../core/helper/Vector3";
import MDN from "../shared/MDN";

class Camera {
  constructor({ gl }) {
    this.gl = gl;
    this.postionVector = new Vactor3({ x: 2, y: 1, z: 1 });
    this.postion = mat4.create();
    this.leftAndRight = 1;
    this.inAndOut = 1;
  }

  getMatrix() {
    const fieldOfView = (90 * Math.PI) / 180; // in radians
    const aspect = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
    const zNear = 0.5;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();
    // note: glmatrix.js always has the first argument
    // as the destination to receive the result.
    return mat4.perspective(projectionMatrix, fieldOfView, aspect, zNear, zFar);
  }

  getViewMatrix() {
    var moveInAndOut = 2 * this.inAndOut * 0.1;
    var moveLeftAndRight = 2 * this.leftAndRight * 0.1;

    // 各个方向移动相机
    var position = MDN.translateMatrix(moveLeftAndRight, 0, 50 + moveInAndOut);

    // 相乘，确保以相反的顺序读取它们
    var matrix = MDN.multiplyArrayOfMatrices([
      // 练习: 旋转相机的视角
      position,
    ]);
    return MDN.invertMatrix(matrix);
  }

  update() {
    var leftAndRight = 0;
    var nearAndFar = 0;
    switch (this.keyCode) {
      case "a":
        this.leftAndRight -= 15;
        break;
      case "d":
        this.leftAndRight += 15;
        break;
      case "w":
        this.inAndOut -= 15;
        break;
      case "s":
        this.inAndOut += 15;
        break;
    }
    this.keyCode = "";
  }

  receiveKey(keyCode) {
    this.keyCode = keyCode;
  }
}

const OpenGlView = () => {
  useEffect(() => {
    InitGl();
  });

  function InitGl() {
    const canvas = document.querySelector("#glcanvas");
    // 初始化WebGL上下文
    const gl = canvas.getContext("webgl");
    const camera = new Camera({ gl: gl });
    // 确认WebGL支持性
    if (!gl) {
      alert("无法初始化WebGL，你的浏览器、操作系统或硬件等可能不支持WebGL。");
      return;
    }

    document.addEventListener("keypress", (event) => {
      switch (event.key) {
        case "a":
        case "d":
        case "w":
        case "s":
          camera.receiveKey(event.key);
          break;
      }
    });

    const list = [];
    const cube = new Cube();

    cube.bind(gl, camera);
    cube.setPosition(-0, 0, -1);
    cube.setScale(0.1, 0.1, 0.1);
    list.push(cube);

    const cube2 = new Cube();
    cube2.bind(gl, camera);
    cube2.setPosition(-10, 0, -10);
    list.push(cube2);

    var timeStep = 20;
    var then = 0;
    var total = 0;

    function render(now) {
      const deltaTime = now - then;
      total += deltaTime;
      if (total - timeStep > 0) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0); // Clear to black, fully opaque
        gl.clearDepth(1.0); // Clear everything
        gl.enable(gl.DEPTH_TEST); // Enable depth testing
        gl.depthFunc(gl.LEQUAL); // Near things obscure far things
        // Clear the canvas before we start drawing on it.
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        camera.update();

        for (let obj of list) {
          obj.setRotate(0.0, 0.01, 0);
          obj.draw();
        }
        total -= timeStep;
      }
      then = now;
      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
  }

  return (
    <canvas id="glcanvas" width="1920" height="1080">
      你的浏览器似乎不支持或者禁用了HTML5 <code>&lt;canvas&gt;</code> 元素.
    </canvas>
  );
};

export default OpenGlView;
