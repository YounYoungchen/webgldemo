/*
 * @Author: Younchen
 * @Description:
 * @Younchen's code.: 匠人精神
 * @Date: 2021-09-30 23:41:22
 * @LastEditTime: 2021-10-06 22:41:24
 */
import "./App.css";
import WebGLSampleView from "./component/WebGLSampleView";
import OpenGlView from "./component/OpenGlView";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="game-ui">
            <div className="menu-container">
              <ui>
                <li>Start</li>
                <li>Setting</li>
                <img src="/static/earth.jpg"></img>
              </ui>
            </div>
          </div>
          <OpenGlView></OpenGlView>
        </div>
      </header>
    </div>
  );
}

export default App;
